var app = require('express')();
var map = {" ": 1,"a": 1,"b": 2,"c": 3,"d": 4,"e": 5,"f": 6,"g": 7,"h": 8,"i": 9,"j": 10,"k": 11,"l": 12,"m": 13,"n": 14,"o": 15,"p": 16,"q": 17,"r": 18,"s": 19,"t": 20,"u": 21,"v": 22,"w": 23,"x": 24,"y": 25,"z": 26,0:1,1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9};
function jumble(text,n) {
var result="";
go(text,n);
function go(text,n) {
if (text.length!=1) {
var p = text.split("")[((n-1)%(text.length))];
result+=p;
go(text.slice(((n-1)%(text.length))+1,text.length)+text.slice(0,((n-1)%(text.length))),n);
}
else {result+=text;}
}
return result;
}   
function unjumble(text,n) {
var result=[];
var nums=[];
var finalresult=[];
for (i=0;i<text.length;i++) {nums[i]=i}
go(nums,n);
function go(nums,n) {
if (nums.length!=1) {
var p = nums[((n-1)%(nums.length))];
result.push(p);
go(nums.slice(((n-1)%(nums.length))+1,nums.length).concat(nums.slice(0,((n-1)%(nums.length)))),n);
}
else {result.push(nums[0])}
}
for (x in result) {finalresult[result[x]]=text[x]}
return finalresult.join("");
}
function encrypt(text,key) {var result=text;for (x in key) {result=jumble(result,parseInt(map[key[x]]))};return result;}
function decrypt(text,key) {key=key.split("").reverse().join("");var result=text;for (x in key) {result=unjumble(result,parseInt(map[key[x]]))};return result;}

app.get("/jumble", function(req,res) {
res.setHeader("Access-Control-Allow-Origin","*");
res.type("text/plain").end(encrypt(req.query.msg,req.query.key))
})

app.get("/unjumble", function(req,res) {
res.setHeader("Access-Control-Allow-Origin","*");
res.type("text/plain").end(decrypt(req.query.msg,req.query.key))
})

app.get("/*", function(req,res) {
res.redirect(301,"https://theabbie.github.io");
})

app.listen(process.env.PORT);
